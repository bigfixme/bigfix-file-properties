﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace BigFix_File_Properties
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string filePath = String.Join(" ", args);

            if (string.IsNullOrEmpty(filePath)) filePath = @"BFProp.exe";

            if (File.Exists(filePath))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                //confirm settings have trailing /
                if (!Properties.Settings.Default.URL.EndsWith("/"))
                {
                    Properties.Settings.Default.URL += "/";
                    Properties.Settings.Default.Save();
                }
                if (!Properties.Settings.Default.Path.EndsWith(@"\"))
                {
                    Properties.Settings.Default.Path += @"\";
                    Properties.Settings.Default.Save();
                }



                //we have the filename... now let's process and fill in the details.
                Form1 f = new Form1(filePath);

                //now display form for user
                Application.Run(f);
            }
        }
    }
}
