﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Diagnostics;
using System.Collections;

namespace BigFix_File_Properties
{

        public class FTP
        {
            public ICredentials cred;
            public bool KeepAlive = false;
            public bool UseSSL = true;
            private string m_FTPSite = "";
            public string FTPSite
            {
                get { return m_FTPSite; }
                set
                {
                    m_FTPSite = value;
                    if (!m_FTPSite.EndsWith("/")) m_FTPSite += "/";
                }
            }



            public FTP() { }

            public FTP(string sFTPSite, ICredentials credentials)
            {
                cred = credentials;
                FTPSite = sFTPSite;
            }





            private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                //this will allow us to use our temporary certificate while leaving the rest of this functionality intact
                if (certificate.Issuer == "E=danielheth@hotmail.com, OU=IT, O=RootSync, L=Springdale, S=Arkansas, C=US, CN=rootsync.com")
                    return true;  //need to remove this... hopefully purchasing an official certificate will correct this issue.

                if (sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors)
                {
                    return false;
                }
                else if (sslPolicyErrors == SslPolicyErrors.RemoteCertificateNameMismatch)
                {
                    System.Security.Policy.Zone z = System.Security.Policy.Zone.CreateFromUrl(((HttpWebRequest)sender).RequestUri.ToString());
                    if (z.SecurityZone == System.Security.SecurityZone.Intranet || z.SecurityZone == System.Security.SecurityZone.MyComputer)
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }


            private List<string> GetFileList(string CurDirectory, string StartsWith, string EndsWith)
            {
                List<string> oList = null;

                try
                {
                    FtpWebRequest oFTP = GetRequest(CurDirectory);

                    // Validate the server certificate with         
                    // ServerCertificateValidationCallBack         
                    if (UseSSL) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                    //System.Security.Cryptography.X509Certificates.        
                    //X509Certificate oCert = new System.Security.Cryptography.         
                    //X509Certificates.X509Certificate();         
                    //oFTP.ClientCertificates.Add(oCert); 

                    oFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                    using (FtpWebResponse response = (FtpWebResponse)oFTP.GetResponse())
                    {
                        using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                        {
                            string str = sr.ReadLine();
                            oList = new List<string>();
                            while (str != null)
                            {
                                if (str.StartsWith(StartsWith) && str.EndsWith(EndsWith)) oList.Add(str);
                                str = sr.ReadLine();
                            }
                        }
                    }
                    oFTP = null;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("GetFileList ERROR:  " + ex.Message);
                }

                return oList;
            }




            public bool DownloadFile(string Name, string DestFile)
            {
                try
                {
                    FtpWebRequest oFTP = GetRequest(Name);
                    oFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                    oFTP.UseBinary = true;

                    if (UseSSL) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);


                    using (FtpWebResponse response = (FtpWebResponse)oFTP.GetResponse())
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            //loop to read & write to file         
                            using (FileStream fs = new FileStream(DestFile, FileMode.Create))
                            {
                                Byte[] buffer = new Byte[2047];
                                int read = 1;
                                while (read != 0)
                                {
                                    read = responseStream.Read(buffer, 0, buffer.Length);
                                    fs.Write(buffer, 0, read);
                                }
                                //see Note(1)         
                                fs.Flush();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("DownloadFile ERROR:  " + ex.Message);
                    return false;
                }
                return true;
            }




            public bool UploadFile(string FullPath, string ftpPath)
            {
                try
                {
                    FileInfo oFile = new System.IO.FileInfo(FullPath);


                    //Settings required to establish a connection with            
                    //the server            
                    FtpWebRequest ftpRequest = GetRequest(ftpPath);
                    ftpRequest.UseBinary = true;
                    ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;

                    // Validate the server certificate with       
                    // ServerCertificateValidationCallBack        
                    if (UseSSL) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                    //Selection of file to be uploaded           
                    byte[] fileContents = new byte[oFile.Length];
                    //will destroy the object immediately after being used      
                    using (FileStream fr = oFile.OpenRead())
                    {
                        fr.Read(fileContents, 0, Convert.ToInt32(oFile.Length));
                    }
                    using (Stream writer = ftpRequest.GetRequestStream())
                    {
                        writer.Write(fileContents, 0, fileContents.Length);
                    }
                    //Gets the FtpWebResponse of the uploading operation      
                    using (FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse())
                    {
                        //Display response        
                        //Response.Write(ftpResponse.StatusDescription); 
                    }
                    ftpRequest = null;
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("UploadFile ERROR:  " + ex.Message);
                    return false;
                    //this.Message = webex.ToString();        
                }
            }






            public bool UploadDirectory(string dirPath)
            {
                FtpWebRequest ftpRequest;

                try
                {
                    //Settings required to establish a connection with            
                    //the server            
                    ftpRequest = GetRequest(dirPath);
                    //ftpRequest.UseBinary = true;
                    ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;

                    // Validate the server certificate with       
                    // ServerCertificateValidationCallBack        
                    if (UseSSL) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                    //Selection of file to be uploaded

                    //Gets the FtpWebResponse of the uploading operation      
                    using (FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse())
                    {
                        //Display response        
                        //Response.Write(ftpResponse.StatusDescription); 
                    }
                    ftpRequest = null;
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("UploadDirectory ERROR:  " + ex.Message);
                    return false;
                }
            }



            public bool RenameItem(string oldPath, string newPath)
            {
                FtpWebRequest ftpRequest;

                try
                {
                    //Settings required to establish a connection with            
                    //the server            
                    ftpRequest = GetRequest(oldPath);
                    //ftpRequest.UseBinary = true;
                    ftpRequest.Method = WebRequestMethods.Ftp.Rename;
                    ftpRequest.RenameTo = newPath;

                    // Validate the server certificate with       
                    // ServerCertificateValidationCallBack        
                    if (UseSSL) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                    //Selection of file to be uploaded

                    //Gets the FtpWebResponse of the uploading operation      
                    using (FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse())
                    {
                        //Display response        
                        //Response.Write(ftpResponse.StatusDescription); 
                    }
                    ftpRequest = null;
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("RenameDirectory ERROR:  " + ex.Message);
                    return false;
                }
            }





            //The master method that deletes the entire directory hierarchy. Root directory is identified by the parameter, dirPath
            // Remember: if the path was ftp://testserver/testfolder/testorder then all the folders inside the testorder folder will be deleted including testorder folder.
            public bool DeleteDirectory(string dirPath)
            {
                try
                {
                    FtpListing fileObj = new FtpListing(); //Create the FTPListing object
                    ArrayList DirectoriesList = new ArrayList(); //Create a collection to hold list of directories within the root and including the root directory.
                    DirectoriesList.Add(dirPath); //Add the root folder to the collection
                    string currentDirectory = string.Empty;

                    //For each of the directories in the DirectoriesListCollection, obtain the
                    // sub directories. Add them to the collection.Repeat the process until every path
                    // was traversed. For example consider the following hierarchy.

                    // Ex: ftp://testftpserver/testfolders/testfolder
                    // Sample Hierarchy of testfolder
                    // testfolder
                    // - testfolder1
                    // - testfolder2
                    // - testfolder3
                    // - testfile.txt
                    // - testfolder4
                    // DirectorieList is a self modifying collection. Meaning it loops through itself and adds directories to itself.
                    // DirectoriesList will have the following entries basing on the above hierarchy by the time the first for loop gets complete executed.
                    //ftp://testftpserver/testfolders/testfolder
                    //ftp://testftpserver/testfolders/testfolder/testfolder1
                    //ftp://testftpserver/testfolders/testfolder/testfolder2
                    //ftp://testftpserver/testfolders/testfolder/testfolder4
                    //ftp://testftpserver/testfolders/testfolder/testfolder1/testfolder3
                    //At the end of the for loop the DirectorLists is traversed bottom up
                    //Meaning deletion of folders will be from the last entry towards to first,
                    //which would ensure that all the subdirectories are deleted before the root folder is going to be deleted.
                    for (int directoryCount = 0; directoryCount < DirectoriesList.Count; directoryCount++)
                    {
                        currentDirectory = DirectoriesList[directoryCount].ToString() + "/";

                        string[] directoryInfo = GetDirectoryList(currentDirectory);
                        for (int counter = 0; counter < directoryInfo.Length; counter++)
                        {
                            string currentFileOrDir = directoryInfo[counter];
                            if (currentFileOrDir.Length < 1) // If all entries were scanned then break
                            {
                                break;
                            }

                            currentFileOrDir = currentFileOrDir.Replace("\r", "");
                            fileObj.GetFtpFileInfo(currentFileOrDir, currentDirectory);
                            if (fileObj.FileType == FtpListing.DirectoryEntryType.Directory)
                            {
                                DirectoriesList.Add(fileObj.FullName); //If Directory add to the collection.
                            }
                            else if (fileObj.FileType == FtpListing.DirectoryEntryType.File)
                            {
                                DeleteFile(fileObj.FullName); //If file,then delete.
                            }
                        }
                    }

                    //Remove the directories in the collection from bottom toward top.
                    //This would ensure that all the sub directories were deleted first before deleting the root folder.
                    for (int count = DirectoriesList.Count; count > 0; count--)
                    {
                        RemoveDirectory(DirectoriesList[count - 1].ToString());
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("DeleteDirectory ERROR:  " + ex.Message);
                    return false;
                }

                return true;
            }


            //Send the command/request to the target FTP location identified by the parameter, URI
            private FtpWebRequest GetRequest(string path)
            {
                FtpWebRequest result = ((FtpWebRequest)(FtpWebRequest.Create(FTPSite + path)));
                result.Credentials = cred;
                result.KeepAlive = KeepAlive;
                result.EnableSsl = UseSSL;
                result.UsePassive = false;
                result.ReadWriteTimeout = 15000;
                result.Proxy = null;

                return result;
            }

            //Gets the response for a given request. Meaning executes the command identified
            // by FtpWebRequest and outputs the response/output.
            private string GetStringResponse(FtpWebRequest ftpRequest)
            {
                //Get the result, streaming to a string
                string result = "";
                using (FtpWebResponse response = ((FtpWebResponse)(ftpRequest.GetResponse()))) //Get the response for the request identified by the parameter ftpRequest
                {
                    using (Stream datastream = response.GetResponseStream())
                    {
                        using (StreamReader sr = new StreamReader(datastream))
                        {
                            result = sr.ReadToEnd();
                        }
                    }
                }
                return result;
            }




            //Deletes one target directory at a time, identified by the parameter, dirPath
            private bool RemoveDirectory(string dirPath)
            {
                try
                {
                    dirPath = dirPath.Trim(new char[] { '/' }); //Trim the path.
                    FtpWebRequest ftp = GetRequest(dirPath); //Prepare the request.
                    if (UseSSL) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                    ftp.Credentials = cred; //Set the Credentials to access the ftp target.
                    ftp.Method = WebRequestMethods.Ftp.RemoveDirectory; //set request method, RemoveDirectory
                    string str = GetStringResponse(ftp); // Fire the command to remove directory.
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("RemoveDirectory ERROR:  " + ex.Message);
                    return false;
                }
                return true;
            }


            //Deletes the target FTP file identified by the parameter, filePath
            public bool DeleteFile(string filePath)
            {
                try
                {
                    string URI = (filePath.TrimEnd());
                    FtpWebRequest ftp = GetRequest(URI);
                    if (UseSSL) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                    ftp.Credentials = cred;
                    ftp.Method = WebRequestMethods.Ftp.DeleteFile;
                    string str = GetStringResponse(ftp);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("DeleteFile ERROR:  " + ex.Message);
                    return false;
                }
                return true;
            }

            //Gets the directory listing given the path
            public string[] GetDirectoryList(string path)
            {
                string[] result = null;
                try
                {
                    FtpWebRequest ftpReq = GetRequest(path.TrimEnd()); //Create the request
                    if (UseSSL) ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                    ftpReq.Method = WebRequestMethods.Ftp.ListDirectoryDetails; //Set the request method
                    ftpReq.Credentials = cred; //Set the credentials
                    FtpWebResponse ftpResp = (FtpWebResponse)ftpReq.GetResponse();//Fire the command
                    Stream ftpResponseStream = ftpResp.GetResponseStream(); //Get the output
                    StreamReader reader = new StreamReader(ftpResponseStream, System.Text.Encoding.UTF8);//Encode the output to UTF8 format
                    result = (reader.ReadToEnd().Split('\n')); //Split the output for newline characters.
                    ftpResp.Close(); //Close the response object.
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("GetDirectoryList ERROR:  " + ex.Message);
                }

                return result; // return the output

            }





















        }




}
