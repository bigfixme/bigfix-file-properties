﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BigFix_File_Properties
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            textBox1.Text = Properties.Settings.Default.URL;
            textBox5.Text = Properties.Settings.Default.FTPServer;
            textBox2.Text = Properties.Settings.Default.Path;

            textBox3.Text = Properties.Settings.Default.Username;
            textBox4.Text = Properties.Settings.Default.Password;


            if (textBox2.Text == @"\") textBox2.Text = "";
            if (textBox5.Text == "/") textBox5.Text = "";



            if (Properties.Settings.Default.useFTP)
                tabControl2.SelectedTab = tabControl2.TabPages[1];
            else tabControl2.SelectedTab = tabControl2.TabPages[0];

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!textBox1.Text.EndsWith("/"))
                textBox1.Text += "/";
            if (!textBox5.Text.EndsWith("/"))
                textBox5.Text += "/";
            if (!textBox2.Text.EndsWith(@"\"))
                textBox2.Text += @"\";


            Properties.Settings.Default.useFTP = (tabControl2.SelectedTab.Text == "FTP Server");

            Properties.Settings.Default.URL = textBox1.Text;
            Properties.Settings.Default.FTPServer = textBox5.Text;
            Properties.Settings.Default.Path = textBox2.Text;
            Properties.Settings.Default.Username = textBox3.Text;
            
            if (Properties.Settings.Default.Password != textBox4.Text)
            {
                SimpleAES aes = new SimpleAES();
                Properties.Settings.Default.Password = aes.EncryptToString(textBox4.Text);
            }


            Properties.Settings.Default.Save();

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowNewFolderButton = true;
            DialogResult dr = folderBrowserDialog1.ShowDialog();

            textBox2.Text = folderBrowserDialog1.SelectedPath;
        }

        
    }
}
