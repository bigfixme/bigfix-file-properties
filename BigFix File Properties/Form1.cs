﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Security.Cryptography;
using System.Net;
using System.Diagnostics;

namespace BigFix_File_Properties
{
    public partial class Form1 : Form
    {
        

        public Form1(string filePath)
        {
            InitializeComponent();

            FileInfo fi = new FileInfo(filePath);


            //process this file.
            

            pName.Text = fi.Name;

            Icon icon = Icon.ExtractAssociatedIcon(filePath);
            this.Icon = icon; 
            pbIcon.Image = icon.ToBitmap();

            this.Text = fi.Name + " - BigFix Properties";

            try
            {
                RegistryKey key = Registry.ClassesRoot.OpenSubKey(fi.Extension);
                pType.Text = key.GetValue("Content Type").ToString();
            }
            catch
            {
                label1.Visible = false;
                pType.Visible = false;
            }
            
            pPath.Text = fi.DirectoryName;
            if (!pPath.Text.EndsWith(@"\")) pPath.Text += @"\";

            pSize.Text = fi.Length.ToString() + " bytes";

            pSHA1.Text = getSHA1(filePath);
            pMD5.Text = getMD5(filePath);

            

            pCreated.Text = fi.CreationTime.ToString();
            pModified.Text = fi.LastWriteTime.ToString();
            pAccessed.Text = fi.LastAccessTime.ToString();


            


        }


        private string getSHA1(string filePath)
        {
            string hashText = "";
            string hexValue = "";

            byte[] fileData = File.ReadAllBytes(filePath);
            byte[] hashData = SHA1.Create().ComputeHash(fileData); // SHA1 or MD5

            foreach (byte b in hashData)
            {
                hexValue = b.ToString("X").ToLower(); // Lowercase for compatibility on case-sensitive systems
                hashText += (hexValue.Length == 1 ? "0" : "") + hexValue;
            }

            return hashText;
        }

        private string getMD5(string filePath)
        {
            string hashText = "";
            string hexValue = "";

            byte[] fileData = File.ReadAllBytes(filePath);
            byte[] hashData = MD5.Create().ComputeHash(fileData); // SHA1 or MD5

            foreach (byte b in hashData)
            {
                hexValue = b.ToString("X").ToLower(); // Lowercase for compatibility on case-sensitive systems
                hashText += (hexValue.Length == 1 ? "0" : "") + hexValue;
            }

            return hashText;

        }





        /// <summary>
        /// User pushed the OK Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
        /// <summary>
        /// User pushed the Cancel Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// User pushed Apply button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnApply_Click(object sender, EventArgs e)
        {

            btnApply.Enabled = false;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.ShowDialog();


            UpdateRepositoryStatus();
            

        }

       



        public void UpdateRepositoryStatus()
        {
            rURL.Text = Properties.Settings.Default.URL;


            bool repositoryError = false;

            //first update GUI with status of path
            if (!Properties.Settings.Default.useFTP)
            {
                string path = Properties.Settings.Default.Path;
                if (!Directory.Exists(Properties.Settings.Default.Path) || Properties.Settings.Default.Path.Trim() == @"\")
                    repositoryError = true;
                else repositoryError = false;
            }
            else
            {
                repositoryError = false;
            }

            
            
            
            if (!repositoryError)
            {
                RepositoryError.Visible = false;

                string publicURL = Properties.Settings.Default.URL + pName.Text.Replace(" ", "%20");

                if (Properties.Settings.Default.useFTP)
                {  //this is an ftp site.  Unless we download the file (unknown size) we can't compare sha1, so just see if file exists

                    if (ValidateFTPCredentials())
                    {

                        string remoteFile = Properties.Settings.Default.FTPServer + pName.Text;

                        FtpListing ftpFile = FTPFileExists(remoteFile);

                        if (ftpFile != null)
                        {
                            pictureBox2.Image = imageList1.Images[0];
                            label7.Text = "Repository File Exists, But Unable to Match via FTP";
                            label13.Visible = false;


                            IFormatProvider culture = System.Globalization.CultureInfo.InvariantCulture;
                            DateTime localDT = DateTime.Parse(pModified.Text);

                            TimeSpan span = DateTime.Parse(localDT.ToString("MM/dd/yyyy hh:mm:00"), culture).Subtract(DateTime.Parse(ftpFile.FileDateTime.ToString("MM/dd/yyyy hh:mm:00"), culture));
                                

                            if (span.Minutes < 2)
                                label20.Text = "Note: Local and FTP files have matching timestamps.";
                            else label20.Text = "Note: Local and FTP files do not have matching timestamps."; 
                            label20.Visible = true;


                            //copy to
                            button2.Visible = true;
                            label10.Visible = true;
                            button2.Text = "Overwrite";

                            //remove
                            button3.Visible = true;
                            label11.Visible = true;

                            //actionscript
                            panel6.Visible = true;
                            label12.Visible = true;
                            textBox1.Visible = true;

                            textBox1.Text = "prefetch " + pName.Text.Replace(" ", "%20") + " sha1:" + pSHA1.Text + " size:" + pSize.Text.Replace(" bytes", "") + " " + publicURL;
                            textBox1.Text += Environment.NewLine + @"waithidden __Download\" + pName.Text;
                        }
                        else
                        {
                            pictureBox2.Image = imageList1.Images[1];
                            label7.Text = "Repository File Does Exist";
                            label13.Visible = false;

                            //copy to
                            button2.Visible = true;
                            label10.Visible = true;
                            button2.Text = "Upload";

                            //remove
                            button3.Visible = false;
                            label11.Visible = false;

                            //actionscript
                            panel6.Visible = false;
                            label12.Visible = false;
                            textBox1.Visible = false;
                        }
                    }
                    else
                    {
                        pictureBox2.Image = imageList1.Images[1];
                        label7.Text = "Respository Properties Invalid";
                        label13.Visible = true;

                        //copy to
                        button2.Visible = false;
                        label10.Visible = false;

                        //remove
                        button3.Visible = false;
                        label11.Visible = false;

                        //actionscript
                        panel6.Visible = false;
                        label12.Visible = false;
                        textBox1.Visible = false;
                    }
                }
                else
                {  //this is a local or network share path... so we can do somethings automatically.

                    string localFile = Properties.Settings.Default.Path + pName.Text;

                    bool fileExists = File.Exists(localFile);

                    if (fileExists)
                    {
                        string sha1 = getSHA1(localFile);

                        bool shaDifferent = (sha1 != pSHA1.Text);

                        if (!shaDifferent)
                        {
                            pictureBox2.Image = imageList1.Images[0];
                            label7.Text = "Matched with Repository File";
                            label13.Visible = false;

                            //copy to
                            button2.Visible = true;
                            label10.Visible = true;
                            button2.Text = "Copy to";

                            //remove
                            button3.Visible = true;
                            label11.Visible = true;

                            //actionscript
                            panel6.Visible = true;
                            label12.Visible = true;
                            textBox1.Visible = true;

                            textBox1.Text = "prefetch " + pName.Text.Replace(" ", "%20") + " sha1:" + pSHA1.Text + " size:" + pSize.Text.Replace(" bytes", "") + " " + publicURL;
                            textBox1.Text += Environment.NewLine + @"waithidden __Download\" + pName.Text;
                        }
                        else
                        {
                            pictureBox2.Image = imageList1.Images[1];
                            label7.Text = "Repository File Does Not Match";
                            label13.Visible = false;

                            //copy to
                            button2.Visible = true;
                            label10.Visible = true;
                            button2.Text = "Overwrite";

                            //remove
                            button3.Visible = true;
                            label11.Visible = true;

                            //actionscript
                            panel6.Visible = false;
                            label12.Visible = false;
                            textBox1.Visible = false;
                        }


                    }
                    else
                    {
                        pictureBox2.Image = imageList1.Images[1];
                        label7.Text = "No Matching Repository File";
                        label13.Visible = false;

                        //copy to
                        button2.Visible = true;
                        label10.Visible = true;
                        button2.Text = "Copy to";

                        //remove
                        button3.Visible = false;
                        label11.Visible = false;

                        //actionscript
                        panel6.Visible = false;
                        label12.Visible = false;
                        textBox1.Visible = false;
                    }

                } //if ftp

            }
            else
            {
                RepositoryError.Visible = true;

                pictureBox2.Image = imageList1.Images[1];
                label7.Text = "Respository Properties Invalid";
                label13.Visible = true;

                //copy to
                button2.Visible = false;
                label10.Visible = false;

                //remove
                button3.Visible = false;
                label11.Visible = false;

                //actionscript
                panel6.Visible = false;
                label12.Visible = false;
                textBox1.Visible = false;
                
            }



        }








        public bool ValidateFTPCredentials()
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.FTPServer)) return false;
            if (!Properties.Settings.Default.FTPServer.ToLower().StartsWith("ftp://")) return false;

            try
            {
                string[] ftpServer = Properties.Settings.Default.FTPServer.Split('/');
                string ftpserverURL = ftpServer[0] + "//" + ftpServer[2] + "/";

                string ftpserverPath = Properties.Settings.Default.FTPServer.Replace(ftpserverURL, "");


                //decrypt the password, if necessary
                string passWord = Properties.Settings.Default.Password;
                SimpleAES aes = new SimpleAES();
                if (aes.isEncoded(passWord))
                { //pw is encrypted, so decrypt it for use
                    passWord = aes.DecryptString(passWord);
                }
                else
                { //pw is NOT encrypted, we should encrypt it and save it back out for safety, before continuing
                    Properties.Settings.Default.Password = aes.EncryptToString(passWord);
                    Properties.Settings.Default.Save();
                }

                //initiate our sftp object with credentials and initiate an onlineSync
                ICredentials cred = new NetworkCredential(Properties.Settings.Default.Username, passWord);
                FTP sftp = new FTP(ftpserverURL, cred);
                sftp.UseSSL = false;


                string[] directoryInfo = sftp.GetDirectoryList(ftpserverPath);

                return (directoryInfo != null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }











        public FtpListing FTPFileExists(string path)
        {
            if (string.IsNullOrEmpty(path)) return null;
            if (!path.ToLower().StartsWith("ftp://")) return null;

            try
            {
                string[] ftpServer = path.Split('/');
                string ftpserverURL = ftpServer[0] + "//" + ftpServer[2] + "/";

                string fileName = ftpServer[ftpServer.Count() - 1];

                string ftpserverPath = Properties.Settings.Default.FTPServer.Replace(ftpserverURL, "").Replace(fileName, "");


                //decrypt the password, if necessary
                string passWord = Properties.Settings.Default.Password;
                SimpleAES aes = new SimpleAES();
                if (aes.isEncoded(passWord))
                { //pw is encrypted, so decrypt it for use
                    passWord = aes.DecryptString(passWord);
                }
                else
                { //pw is NOT encrypted, we should encrypt it and save it back out for safety, before continuing
                    Properties.Settings.Default.Password = aes.EncryptToString(passWord);
                    Properties.Settings.Default.Save();
                }

                //initiate our sftp object with credentials and initiate an onlineSync
                ICredentials cred = new NetworkCredential(Properties.Settings.Default.Username, passWord);
                FTP sftp = new FTP(ftpserverURL, cred);
                sftp.UseSSL = false;


                string[] directoryInfo = sftp.GetDirectoryList(ftpserverPath);

                foreach (string dInfo in directoryInfo)
                {
                    string currentFileOrDir = dInfo.Replace("\r", "");

                    if (currentFileOrDir.ToLower().Contains(fileName.ToLower()))
                    {
                        FtpListing fileObj = new FtpListing();
                        fileObj.GetFtpFileInfo(currentFileOrDir, ftpserverPath);


                        if (fileObj.FileType == FtpListing.DirectoryEntryType.File)
                            return fileObj;
                    }
                }
                                                        
                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

        }







        public bool uploadFTPFile(string localPath, string remotePath)
        {
            if (string.IsNullOrEmpty(remotePath) || string.IsNullOrEmpty(localPath)) return false;
            if (!remotePath.ToLower().StartsWith("ftp://")) return false;
            if (!File.Exists(localPath)) return false;

            try
            {
                string[] ftpServer = remotePath.Split('/');
                string ftpserverURL = ftpServer[0] + "//" + ftpServer[2] + "/";

                string fileName = ftpServer[ftpServer.Count() - 1];

                string ftpserverPath = Properties.Settings.Default.FTPServer.Replace(ftpserverURL, "").Replace(fileName, "");


                //decrypt the password, if necessary
                string passWord = Properties.Settings.Default.Password;
                SimpleAES aes = new SimpleAES();
                if (aes.isEncoded(passWord))
                { //pw is encrypted, so decrypt it for use
                    passWord = aes.DecryptString(passWord);
                }
                else
                { //pw is NOT encrypted, we should encrypt it and save it back out for safety, before continuing
                    Properties.Settings.Default.Password = aes.EncryptToString(passWord);
                    Properties.Settings.Default.Save();
                }

                //initiate our sftp object with credentials and initiate an onlineSync
                ICredentials cred = new NetworkCredential(Properties.Settings.Default.Username, passWord);
                FTP sftp = new FTP(ftpserverURL, cred);
                sftp.UseSSL = false;

                sftp.DeleteFile(ftpserverPath + fileName);

                return sftp.UploadFile(localPath, ftpserverPath + fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }





        public bool deleteFTPFile(string remotePath)
        {
            if (string.IsNullOrEmpty(remotePath)) return false;
            if (!remotePath.ToLower().StartsWith("ftp://")) return false;

            try
            {
                string[] ftpServer = remotePath.Split('/');
                string ftpserverURL = ftpServer[0] + "//" + ftpServer[2] + "/";

                string fileName = ftpServer[ftpServer.Count() - 1];

                string ftpserverPath = Properties.Settings.Default.FTPServer.Replace(ftpserverURL, "").Replace(fileName, "");


                //decrypt the password, if necessary
                string passWord = Properties.Settings.Default.Password;
                SimpleAES aes = new SimpleAES();
                if (aes.isEncoded(passWord))
                { //pw is encrypted, so decrypt it for use
                    passWord = aes.DecryptString(passWord);
                }
                else
                { //pw is NOT encrypted, we should encrypt it and save it back out for safety, before continuing
                    Properties.Settings.Default.Password = aes.EncryptToString(passWord);
                    Properties.Settings.Default.Save();
                }

                //initiate our sftp object with credentials and initiate an onlineSync
                ICredentials cred = new NetworkCredential(Properties.Settings.Default.Username, passWord);
                FTP sftp = new FTP(ftpserverURL, cred);
                sftp.UseSSL = false;


                return sftp.DeleteFile(ftpserverPath + fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }










        private void button2_Click(object sender, EventArgs e)
        {
            if (!Properties.Settings.Default.useFTP)
            {
                try
                {
                    File.Copy(pPath.Text + pName.Text, Properties.Settings.Default.Path + pName.Text, true);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                uploadFTPFile(pPath.Text + pName.Text, Properties.Settings.Default.FTPServer + pName.Text);
            }

            UpdateRepositoryStatus();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!Properties.Settings.Default.useFTP)
            {
                try
                {
                    File.Delete(Properties.Settings.Default.Path + pName.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                deleteFTPFile(Properties.Settings.Default.FTPServer + pName.Text);
            }

            UpdateRepositoryStatus();
        }










        private void Form1_Load(object sender, EventArgs e)
        {
            label17.Text += " v" + Application.ProductVersion;


            UpdateRepositoryStatus();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://bigfix.me/BigFixFileProperties");
        }





    }
}
